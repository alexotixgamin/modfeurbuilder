package net.mcreator.feurbuilder.procedures;

import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.InteractionResult;
import net.minecraft.core.BlockPos;

import net.mcreator.feurbuilder.init.FeurBuilderModBlocks;

public class ClickStrippedCyanProcedure {
	public static InteractionResult execute(LevelAccessor world, double x, double y, double z, Entity entity) {
		if (entity == null)
			return InteractionResult.PASS;
		if ((entity instanceof LivingEntity _livEnt ? _livEnt.getMainHandItem() : ItemStack.EMPTY).getItem() == Items.IRON_AXE) {
			world.setBlock(new BlockPos(x, y, z), FeurBuilderModBlocks.STRIPPED_CYAN_LOG.get().defaultBlockState(), 3);
		}
		if ((entity instanceof LivingEntity _livEnt ? _livEnt.getMainHandItem() : ItemStack.EMPTY).getItem() == Items.DIAMOND_AXE) {
			world.setBlock(new BlockPos(x, y, z), FeurBuilderModBlocks.STRIPPED_CYAN_LOG.get().defaultBlockState(), 3);
		}
		if ((entity instanceof LivingEntity _livEnt ? _livEnt.getMainHandItem() : ItemStack.EMPTY).getItem() == Items.NETHERITE_AXE) {
			world.setBlock(new BlockPos(x, y, z), FeurBuilderModBlocks.STRIPPED_CYAN_LOG.get().defaultBlockState(), 3);
		}
		return InteractionResult.PASS;
	}
}
